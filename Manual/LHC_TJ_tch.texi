@node t-channel single-top production
@subsection t-channel single-top production

@example
@smallformat
@verbatiminclude Examples/SingleTop_Channels/Sherpa.tj-t_channel.yaml
@end smallformat
@end example

Things to notice:
@itemize
@item We use OpenLoops to compute the virtual corrections @mycite{Cascioli2011va}.
@item We match matrix elements and parton showers using the MC@@NLO technique 
for massive particles, as described in @mycite{Hoeche2013mua}.
@item A non-default METS core scale setter is used, cf. @ref{METS scale setting with multiparton core processes}
@item We enable top and W decays through the internal decay module using
@option{HARD_DECAYS:Enabled: true}.
The W is restricted to its leptonic decay channels.
@item By setting @option{Min_N_TChannels: 1}, only t-channel diagrams are used
for the calculation
@end itemize


@node t-channel single-top production with N_f=4
@subsection t-channel single-top production with N_f=4

@example
@smallformat
@verbatiminclude Examples/SingleTop_Channels/Sherpa.tj-t_channel-nf4.yaml
@end smallformat
@end example

Things to notice:
@itemize
@item We use an Nf4 PDF and use its definition of the bottom mass
@item See @ref{t-channel single-top production} for more comments
@end itemize
